var validateForm = function () {
    var name = document.querySelector('#name');
    var age = document.querySelector('#age');
    var dni = document.querySelector('#dni');
    if (validateAge(parseInt(age.value)) && validateName(name.value) && validateDNI(dni.value)) {
        alert('FORMULARIO VALIDADO');
        return true;
    }
    else {
        return false;
    }
};
var validateAge = function (age) {
    if (age < 18) {
        alert('No se admiten menores de 18');
    }
    else if (age >= 100) {
        alert('Demasiado viejo');
    }
    else {
        alert('Edad correcta');
        return true;
    }
    document.getElementById('age').style.color = 'red';
    return false;
};
var validateName = function (name) {
    var initial = name.charAt(0);
    if (initial === initial.toLocaleUpperCase()) {
        alert('Nombre correcto');
        return true;
    }
    else {
        alert('La primera letra en mayúsculas vaquero');
        document.getElementById('name').style.color = 'red';
        return false;
    }
};
var validateDNI = function (dni) {
    var numero;
    var letr;
    var letra;
    var expresion_regular_dni;
    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
    if (expresion_regular_dni.test(dni) == true) {
        numero = parseInt(dni.substr(0, dni.length - 1));
        letr = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra == letr.toUpperCase()) {
            alert('Dni correcto');
            return true;
        }
        else {
            document.getElementById('dni').style.color = 'red';
            alert('Dni mal introducido o inexistente');
            return false;
        }
    }
};
