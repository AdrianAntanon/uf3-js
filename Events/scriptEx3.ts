const validateForm = () => {
    const name: HTMLInputElement = document.querySelector('#name');
    const age: HTMLInputElement = document.querySelector('#age');
    const dni: HTMLInputElement = document.querySelector('#dni');
    if (validateAge(parseInt(age.value)) && validateName(name.value) && validateDNI(dni.value)) {
        alert('FORMULARIO VALIDADO');
        return true;
    } else {
        return false;
    }
};

const validateAge = (age: number) => {
    if (age < 18) {
        alert('No se admiten menores de 18');
    } else if (age >= 100) {
        alert('Demasiado viejo');
    } else {
        alert('Edad correcta');
        return true;
    }
    document.getElementById('age').style.color = 'red';
    return false;
};
const validateName = (name: string) => {
    const initial: string = name.charAt(0);

    if (initial === initial.toLocaleUpperCase()) {
        alert('Nombre correcto');
        return true;
    } else {
        alert('La primera letra en mayúsculas vaquero');
        document.getElementById('name').style.color = 'red';
        return false;
    }
};
const validateDNI = (dni: string) => {
    let numero: number;
    let letr: string;
    let letra: string;
    let expresion_regular_dni: RegExp;

    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;

    if (expresion_regular_dni.test(dni) == true) {
        numero = parseInt(dni.substr(0, dni.length - 1));
        letr = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);

        if (letra == letr.toUpperCase()) {
            alert('Dni correcto');
            return true;
        } else {
            document.getElementById('dni').style.color = 'red';
            alert('Dni mal introducido o inexistente');
            return false;
        }
    }
};
