const BASE_URL =
  "http://lb.cercalia.com/services/json?key=138aae8f9e2586cf8ff4b872cd26eee16825ee9c92ab932fddbfc6d49daabb28&";

const dataUserName = document.querySelectorAll(".userName input");
const dataUserEmail = document.querySelectorAll(".userEmail input");
const dataAddress = document.querySelectorAll(".addressUserInfo input");
const dataLanguage = document.querySelectorAll(".languageSelection select");
const dataGenre = document.querySelectorAll(".userGenre input");
const dataMoreInfo = document.querySelector("textarea");

let map = L.map("map").setView([51.5285582, -0.2416797], 9);
L.tileLayer(
  "https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=rLrIWv6QRXTJu2nodK7n",
  {
    tileSize: 512,
    zoomOffset: -1,
    minZoom: 1,
    attribution:
      '\u003ca href="https://www.maptiler.com/copyright/" target="_blank"\u003e\u0026copy; MapTiler\u003c/a\u003e \u003ca href="https://www.openstreetmap.org/copyright" target="_blank"\u003e\u0026copy; OpenStreetMap contributors\u003c/a\u003e',
    crossOrigin: true,
  }
).addTo(map);

const findAddress = (event) => {
  event.preventDefault();

  fetch(
    `${BASE_URL}cmd=cand&mode=1&detcand=1&cleanadr=1&fadr=${dataAddress[0].value} ${dataAddress[1].value}, ${dataAddress[4].value} ${dataAddress[2].value}&ctryc=ESP&priorityfilter=1`
  )
    .then((response) => response.json())
    .then((data) => {
      const y = data.cercalia.candidates.candidate[0].ge.coord.y;
      const x = data.cercalia.candidates.candidate[0].ge.coord.x;

      map.setView([y, x], 15);
    });
};

const handleSubmit = () => {
  console.log(
    "Tu dirección: ",
    dataAddress[0].value,
    dataAddress[1].value,
    dataAddress[2].value,
    dataAddress[3].value,
    dataAddress[4].value
  );

  // Comprobación del nombre
  const userName = dataUserName[0].value;
  const userLastName = dataUserName[1].value;

  const confirmName =
    userName === "" || userLastName === ""
      ? "Es obligatorio rellenar nombre y apellido"
      : "";

  // Comprobación del idioma
  const languageSelected = dataLanguage[0].value;

  const confirmLanguage =
    languageSelected === "escoge" ? "Escoge un idioma" : "";

  // Comprobación del género
  const male = dataGenre[0].checked;
  const female = dataGenre[0].checked;
  const confirmGenre =
    !male && !female
      ? "Debes de seleccionar un género, los androides no son bienvenidos!"
      : "";
  // if (!male && !female) {
  //   console.log('Funciona');
  //   'Debes de seleccionar un género, los androides no son bienvenidos!'
  // }

  // Comprobación del textarea
  const textareaLength = dataMoreInfo.value.length;

  const confirmTextArea =
    textareaLength > 10 ? "" : "Es necesario añadir información de más";

  const requiredFields = `Campos requeridos: \n${confirmName}, ${confirmLanguage}, ${confirmGenre}, ${confirmTextArea}`;

  if (
    confirmName !== "" ||
    confirmLanguage !== "" ||
    confirmGenre !== "" ||
    confirmTextArea !== ""
  ) {
    alert(requiredFields);
  }
};
